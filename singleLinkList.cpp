#include "singleLinkList.h"

using namespace std;

/**
 * Node class implementation
 */


Node::Node(data_t data) {
  this->setData(data);
  this->setNextNode(NULL);
}

void Node::setData(data_t data) {
  this->data = data;
}

data_t Node::getData(void) {
  return this->data;
}

Node* Node::getNextNode(void) {
  return this->nextNode;
}

void Node::setNextNode(Node* nextNode) {
  this->nextNode = nextNode;
}


/**
 * singleLinkList class implementation
 */


singleLinkList::singleLinkList() {
  this->head = NULL;
  this->isValid.unlock();
}

singleLinkListRet_t singleLinkList::addNodeAtEnd(data_t data) {
  Node *tempHead = this->head;
  Node *tempNode = new Node(data);

  if(tempNode == NULL) {
    //TODO: print error log to convey error message
    return RET_ERR_NO_MEM_AVAILABLE;    
  }

  this->isValid.lock();
  // If the list is empty place the node at head
  if(this->head == NULL) {
    this->head = tempNode;
    this->isValid.unlock();
    return RET_NO_ERR; 
  }

  while(tempHead->getNextNode() != NULL) {
    tempHead = tempHead->getNextNode();
  }

  tempHead->setNextNode(tempNode);
  this->isValid.unlock();

  return RET_NO_ERR;
}


void singleLinkList::displayListUnsafe(void) {
  Node *tempHead = this->head;

  cout << "List Data:" << endl;
  if(tempHead == NULL) {
    cout << "List is empty" << endl;
    return;
  }

  while(tempHead != NULL) {
    cout << "  " << tempHead->getData() << endl;
    tempHead = tempHead->getNextNode();
  }
}

void singleLinkList::displayList(void) {
  this->isValid.lock();
  this->displayListUnsafe();
  this->isValid.unlock();
}

/**
 * Print list in reverse order
 *
 * The function will perform below steps:
 *   1. Reverse the list
 *   2. Display list
 *   3. Reverse the list
 *
 * Read  README.md file for more information
 */
void singleLinkList::displayReverseList(void) {
  /* As list is getting reversed, restrict access of the list */
  this->isValid.lock();

  this->reverseUnsafe();
  this->displayListUnsafe();
  this->reverseUnsafe();

  this->isValid.unlock();
}

void singleLinkList::reverseUnsafe(void) {
  Node *currentNode = this->head;
  Node *nextNode = NULL;
  Node *prevNode = NULL;

  while (currentNode != NULL) {
    nextNode = currentNode->getNextNode();
    currentNode->setNextNode(prevNode);
    prevNode = currentNode;
    currentNode = nextNode;
  }

  this->head = prevNode;
}
