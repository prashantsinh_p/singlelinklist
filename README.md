
Below approaches can be implemented for displayReverseList:

1.recursion:
-------------
It's not a robust way, if node size gets increased at some point stack may get over limit.

2.Use vector or list:
----------------------
In one iteration we can fill the list/vector and print it in reverse order, but it will require more storage, and the idea of using more memory conflicts with the motive of link list - as link list is recommended when we don't want to use more contiguous memory (So vector is out of list here, vector requires continuous memory). We can go with a list if memory is not an issue.

3.Reverse the list:
--------------------
It will have an impact on performance, as we have to first reverse the list then print it and again reverse it to make the list in original order.

So there is a tradeoff between Robustness & Memory vs performance. In my opinion, Reversing the list is matching the current requirement.

**Note:** There are certain things which can be implemented to make the program more modular i.e, using a template to create a Node class, but as per the task, I have just focused on the display print function. Other things I have implemented so that code can be tested.

TODOs:
-----
1. Use logger with debug level instead of cout
2. Use template to create Node class, which can be re-usable
3. Delete the list before exit


Build & Run:
------------
make
./singleLinkList
