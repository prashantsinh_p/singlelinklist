#ifndef SINGLELINKLIST_H 
#define SINGLELINKLIST_H 

#include<iostream>  
#include <mutex>
 
typedef int data_t;           /**< opaque for Node data, better solution to go with the Templates*/

typedef enum {
  RET_NO_ERR = 0,             /**< No error*/
  RET_ERR_NO_MEM_AVAILABLE    /**< Not enough memory */
} singleLinkListRet_t;

class Node { 

private: 
  data_t data;                /**< Node data */ 
  Node* nextNode;             /**< Next node pointer */

public:
  /**
   * Construct object and intialize data and next node
   */
  Node(data_t data);

  // data member
  data_t getData(void);
  void setData(int data);

  // nextNode meber
  Node* getNextNode(void);
  void setNextNode(Node* nextNode);
 
};

class singleLinkList {
private:
  Node *head;                     /**< Link list head */
  std::mutex isValid;             /**< Restrict list access if list is not in valid state */
  void displayListUnsafe(void);   /**< Display function print's without checking valid state */
  void reverseUnsafe(void);       /**< Reverse list without checking valid state */

public:
  /**
   * Intialize list head
   */
  singleLinkList(); 

  /**
   * Add node at end of the list
   */
  singleLinkListRet_t addNodeAtEnd(data_t data);

  /**
   * Display list
   */
  void displayList(void);

  /**
   * Display list in reverse order
   */
  void displayReverseList(void);

  // TODO: Delete list

};
#endif // SINGLELINKLIST_H
