#include "singleLinkList.h"

using namespace std;

int main() {
  singleLinkList list;

  /* Add nodes in the list */
  list.addNodeAtEnd(10);
  list.addNodeAtEnd(20);
  list.addNodeAtEnd(30);
  list.addNodeAtEnd(40);
  list.addNodeAtEnd(50);

  /* Display list in reverse order */
  list.displayReverseList();

  /* Verify list should be in the proper order as data inserted*/
  list.displayList();

  // TODO: List should be deleted here
  return 0;
}
